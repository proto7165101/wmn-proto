.EXPORT_ALL_VARIABLES:
ETCD_ADDR=localhost:2379
APP_NAME=wnm-proto


gogo:
	@protoc -I proto \
  	--go_out proto --go_opt paths=source_relative \
  	--go-grpc_out proto --go-grpc_opt paths=source_relative \
  	--grpc-gateway_out proto --grpc-gateway_opt paths=source_relative \
  	proto/*.proto

gomod:
	@go mod tidy && go mod vendor


#curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/annotations.proto > proto/google/api/annotations.proto
#curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/http.proto > proto/google/api/http.proto



#curl --location --request POST 'http://localhost:5000/v1/ping' \--header 'Content-Type: application/json' \--data-raw '{"name": "everyone"}'